using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain;
using Otus.Teaching.Pcf.Administration.DataAccess.Mongo.Contexts;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Mongo.Repository
{
    public class MongoRepository<T> : IRepository<T> where T: BaseEntity
    {

        private readonly IDataContext _context;
        private IMongoCollection<T> _collection;
        public MongoRepository(IDataContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(_context));
            _collection = _context.GetCollection<T>($"{typeof(T).Name}Collection");
        }


        public async Task AddAsync(T entity)
        {
            await _collection.InsertOneAsync(entity);
        }

        public async Task DeleteAsync(T entity)
        {
            FilterDefinition<T> filter = Builders<T>.Filter.Eq(m => m.Id, entity.Id);
            DeleteResult deleteResult = await _collection.DeleteOneAsync(filter);

            var result = deleteResult.IsAcknowledged
                && deleteResult.DeletedCount > 0;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _collection
                            .Find(p => true)
                            .ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
             return await _collection
                            .Find(p => p.Id == id)
                            .FirstOrDefaultAsync();
        }

        public async Task<T> GetFirstWhere(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return await _collection
                            .Find<T>(predicate)
                            .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            return await _collection
                            .Find<T>(t => ids.Contains(t.Id))
                            .ToListAsync();
        }

        public async Task<IEnumerable<T>> GetWhere(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return await _collection
                            .Find<T>(predicate)
                            .ToListAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            var updateResult = await _collection
                                        .ReplaceOneAsync(filter: g => g.Id == entity.Id, replacement: entity);

            var result = updateResult.IsAcknowledged
                    && updateResult.ModifiedCount > 0;
        }
    }
}