namespace Otus.Teaching.Pcf.Administration.DataAccess.Mongo.Settings
{
    public interface IDbConnectionOptions
    {
        string ConnectionString { get;}
        string DatabaseName { get; }
        string Login {get; }
        string Password {get ;}
    }
}