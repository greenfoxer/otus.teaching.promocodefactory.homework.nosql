namespace Otus.Teaching.Pcf.Administration.DataAccess.Mongo.Settings
{
    public class DbConnectionOptions : IDbConnectionOptions
    {
        public string ConnectionString {get;set;}
        public string DatabaseName  {get;set;}

        public string Login {get;set;}

        public string Password {get;set;}
    }
}