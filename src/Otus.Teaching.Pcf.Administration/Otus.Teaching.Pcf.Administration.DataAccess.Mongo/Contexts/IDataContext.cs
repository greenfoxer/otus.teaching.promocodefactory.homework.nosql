using MongoDB.Driver;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Mongo.Contexts
{
    public interface IDataContext
    {
        IMongoCollection<T> GetCollection<T>(string name);
    }
}