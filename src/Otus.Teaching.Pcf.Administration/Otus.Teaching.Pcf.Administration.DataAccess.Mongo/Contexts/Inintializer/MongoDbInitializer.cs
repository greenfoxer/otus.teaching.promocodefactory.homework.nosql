using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Mongo.Contexts
{
    public class MongoDbInitializer : IDbInitializer
    {
        IDataContext _context;
        public MongoDbInitializer(IDataContext context)
        {
            _context = context;
        }
        public void InitializeDb()
        {
            var roles = _context.GetCollection<Role>($"{nameof(Role)}Collection");
            if (!roles.Find<Role>(s=>true).Any())
            {
                roles.InsertMany(FakeDataFactory.Roles);
            }

            var employee = _context.GetCollection<Employee>($"{nameof(Employee)}Collection");
            if (!employee.Find<Employee>(s=>true).Any())
            {
                employee.InsertMany(FakeDataFactory.Employees);
            }
        }
    }
}