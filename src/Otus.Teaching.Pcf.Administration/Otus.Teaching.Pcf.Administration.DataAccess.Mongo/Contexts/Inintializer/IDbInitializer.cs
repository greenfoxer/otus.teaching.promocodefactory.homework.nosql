﻿namespace Otus.Teaching.Pcf.Administration.DataAccess.Mongo.Contexts
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}