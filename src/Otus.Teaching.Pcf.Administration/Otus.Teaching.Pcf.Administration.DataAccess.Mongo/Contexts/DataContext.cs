using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess.Mongo.Settings;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Mongo.Contexts
{
    public class DataContext : IDataContext
    {
        IMongoDatabase _database;
        IDbConnectionOptions _settings;
        MongoClient _client;
        public DataContext(IDbConnectionOptions settings)
        {
            _settings = settings;
            _client = new MongoClient($@"mongodb://{_settings.Login}:{_settings.Password}@{_settings.ConnectionString}");
            _database = _client.GetDatabase(_settings.DatabaseName);

            // Roles = database.GetCollection<Role>(settings.RoleCollectionName);
            // Employees = database.GetCollection<Employee>(settings.EmployeeCollectionName);
            //DataSeed.SeedData(Products);
        }
        public IMongoCollection<T> GetCollection<T>(string name)
        {
            return _database.GetCollection<T>(name);
        }

    }
}